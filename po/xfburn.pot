# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR The Xfce development team.
# This file is distributed under the same license as the xfburn package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfburn 0.7.2\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/apps/xfburn\n"
"POT-Creation-Date: 2024-08-12 21:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: xfburn/xfburn-global.h:28
msgid "Capacity:"
msgstr ""

#: xfburn/xfburn-global.h:29
msgid "addr:"
msgstr ""

#: xfburn/xfburn-global.h:30
msgid "Time total:"
msgstr ""

#: xfburn/xfburn-global.h:32
msgid "length"
msgstr ""

#: xfburn/xfburn-global.h:33 xfburn/xfburn-copy-cd-progress-dialog.c:93
msgid "Flushing cache..."
msgstr ""

#: xfburn/xfburn-global.h:34
msgid "Please insert a recordable disc and hit enter"
msgstr ""

#: xfburn/xfburn-global.h:35
msgid "Cannot determine disc status - hit enter to try again."
msgstr ""

#: xfburn/xfburn-global.h:36
msgid "CD copying finished successfully."
msgstr ""

#: xfburn/xfburn-global.h:43
msgid "Data composition"
msgstr ""

#: xfburn/xfburn-adding-progress.c:83
msgid "Adding files to the composition"
msgstr ""

#: xfburn/xfburn-adding-progress.c:97
msgid "Cancel"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:70
msgid "Quick Blank"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:71
msgid "Full Blank (slow)"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:72
msgid "Quick Format"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:73
msgid "Full Format"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:74
msgid "Quick Deformat"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:75
msgid "Full Deformat (slow)"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:119
msgid "Eject the disc"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:120
msgid "Default value for eject checkbox"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:166
msgid "Blank Disc"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:175
#: xfburn/xfburn-burn-data-composition-base-dialog.c:158
#: xfburn/xfburn-burn-image-dialog.c:178 xfburn/xfburn-copy-cd-dialog.c:107
#: xfburn/xfburn-copy-dvd-dialog.c:104
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:123
msgid "Burning device"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:188 xfburn/xfburn-device-box.c:158
msgid "Blank mode"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:196
#: xfburn/xfburn-burn-data-composition-base-dialog.c:190
#: xfburn/xfburn-burn-image-dialog.c:186 xfburn/xfburn-copy-cd-dialog.c:115
#: xfburn/xfburn-copy-dvd-dialog.c:112
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:157
msgid "Options"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:200
#: xfburn/xfburn-burn-data-composition-base-dialog.c:194
#: xfburn/xfburn-burn-image-dialog.c:190 xfburn/xfburn-copy-cd-dialog.c:119
#: xfburn/xfburn-copy-dvd-dialog.c:116
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:161
msgid "E_ject disk"
msgstr ""

#. action buttons
#.
#. align = gtk_alignment_new (0, 0, 0, 0);
#. gtk_alignment_set_padding (GTK_ALIGNMENT (align), 0, 0, BORDER * 4, 0);
#. gtk_widget_show (align);
#. gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, FALSE, 0);
#.
#. action buttons
#: xfburn/xfburn-blank-dialog.c:206
#: xfburn/xfburn-burn-data-composition-base-dialog.c:247
#: xfburn/xfburn-burn-image-dialog.c:215 xfburn/xfburn-copy-cd-dialog.c:165
#: xfburn/xfburn-copy-dvd-dialog.c:162 xfburn/xfburn-utils.c:132
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:182
msgid "_Cancel"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:210
msgid "_Blank"
msgstr ""

#. blanking can only be performed on blank discs, format and deformat are allowed to be blank ones
#: xfburn/xfburn-blank-dialog.c:326
msgid "The inserted disc is already blank."
msgstr ""

#. these ones we can blank
#: xfburn/xfburn-blank-dialog.c:332
msgid "Ready"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:335
msgid "No disc detected in the drive."
msgstr ""

#: xfburn/xfburn-blank-dialog.c:344
msgid "Disc is not erasable."
msgstr ""

#: xfburn/xfburn-blank-dialog.c:384
msgid "Blanking disc..."
msgstr ""

#: xfburn/xfburn-blank-dialog.c:411
#: xfburn/xfburn-burn-data-composition-base-dialog.c:479
#: xfburn/xfburn-perform-burn.c:480
msgid "Done"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:414 xfburn/xfburn-perform-burn.c:487
#: xfburn/xfburn-progress-dialog.c:637
msgid "Failure"
msgstr ""

#: xfburn/xfburn-blank-dialog.c:431
#: xfburn/xfburn-burn-data-composition-base-dialog.c:585
#: xfburn/xfburn-burn-image-dialog.c:389 xfburn/xfburn-burn-image-dialog.c:567
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:359
msgid "Unable to grab the drive."
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:128
msgid "Image"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:130
msgid "Show volume name"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:130
msgid "Show a text entry for the name of the volume"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:148
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:113
msgid "Burn Composition"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:169
msgid "Composition name"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:174
msgid "<small>Would you like to change the default composition name?</small>"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:199
#: xfburn/xfburn-burn-image-dialog.c:195 xfburn/xfburn-copy-cd-dialog.c:124
#: xfburn/xfburn-copy-dvd-dialog.c:121
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:166
msgid "_Dummy write"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:203
#: xfburn/xfburn-burn-image-dialog.c:199
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:170
msgid "Burn_Free"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:208
#: xfburn/xfburn-burn-image-dialog.c:204
msgid "Stream _Recording"
msgstr ""

#. create ISO ?
#: xfburn/xfburn-burn-data-composition-base-dialog.c:214
#: xfburn/xfburn-copy-cd-dialog.c:133 xfburn/xfburn-copy-dvd-dialog.c:130
msgid "Only create _ISO"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:251
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:186
msgid "_Burn Composition"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:437
#, c-format
msgid "Could not create destination ISO file: %s"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:444
msgid "Writing ISO..."
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:461
#, c-format
msgid "An error occurred while writing ISO: %s"
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:517
#: xfburn/xfburn-burn-data-composition-base-dialog.c:523
msgid "An error occurred in the burn backend."
msgstr ""

#: xfburn/xfburn-burn-data-composition-base-dialog.c:546
#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:290
msgid "The write mode is not supported currently."
msgstr ""

#. could not create source
#: xfburn/xfburn-burn-data-composition-base-dialog.c:629
msgid "Could not create ISO source structure."
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:135
msgid "Burn image"
msgstr ""

#. file
#: xfburn/xfburn-burn-image-dialog.c:140 xfburn/xfburn-burn-image-dialog.c:163
msgid "Image to burn"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:144
msgid "All files"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:148
msgid "Disc images"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:209
msgid "_Quit after success"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:219
msgid "_Burn image"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:272
msgid "Burn mode is not currently implemented."
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:323 xfburn/xfburn-burn-image-dialog.c:352
msgid "An error occurred in the burn backend"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:336
msgid "Unable to determine image size."
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:343
msgid "Cannot open image."
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:380
msgid "Burning image..."
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:453
msgid ""
"<span weight=\"bold\" foreground=\"darkred\" stretch=\"semiexpanded\">Please "
"select an image to burn</span>"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:493
msgid ""
"Cannot append data to multisession disc in this write mode (use TAO instead)"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:497
msgid "Closed disc with data detected. Need blank or appendable disc"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:499
msgid "No disc detected in drive"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:501
msgid "Cannot recognize state of drive and disc"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:513
msgid "The selected image does not fit on the inserted disc"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:517
msgid "Failed to get image size"
msgstr ""

#: xfburn/xfburn-burn-image-dialog.c:546
msgid ""
"Make sure you selected a valid file and you have the proper permissions to "
"access it."
msgstr ""

#: xfburn/xfburn-compositions-notebook.c:144
msgid "Audio composition"
msgstr ""

#: xfburn/xfburn-compositions-notebook.c:205
msgid "Welcome"
msgstr ""

#: xfburn/xfburn-copy-cd-dialog.c:88
#: xfburn/xfburn-copy-cd-progress-dialog.c:156
msgid "Copy data CD"
msgstr ""

#: xfburn/xfburn-copy-cd-dialog.c:99
msgid "CD Reader device"
msgstr ""

#: xfburn/xfburn-copy-cd-dialog.c:128 xfburn/xfburn-copy-dvd-dialog.c:125
msgid "On the _fly"
msgstr ""

#: xfburn/xfburn-copy-cd-dialog.c:169
msgid "_Copy CD"
msgstr ""

#: xfburn/xfburn-copy-cd-progress-dialog.c:105
msgid "Please insert a recordable disc."
msgstr ""

#: xfburn/xfburn-copy-cd-progress-dialog.c:127
msgid "Writing CD..."
msgstr ""

#: xfburn/xfburn-copy-cd-progress-dialog.c:140
#: xfburn/xfburn-create-iso-progress-dialog.c:102
msgid "Reading CD..."
msgstr ""

#: xfburn/xfburn-copy-dvd-dialog.c:85
msgid "Copy data DVD"
msgstr ""

#: xfburn/xfburn-copy-dvd-dialog.c:96
msgid "DVD Reader device"
msgstr ""

#: xfburn/xfburn-copy-dvd-dialog.c:166
msgid "_Copy DVD"
msgstr ""

#: xfburn/xfburn-create-iso-progress-dialog.c:117
msgid "Create ISO from CD"
msgstr ""

#: xfburn/xfburn-data-composition.c:310 xfburn/xfburn-utils.c:184
#: xfburn/xfburn-utils.c:186 xfburn/xfburn-audio-composition.c:330
msgid "Add"
msgstr ""

#: xfburn/xfburn-data-composition.c:310 xfburn/xfburn-audio-composition.c:330
msgid "Add the selected file(s) to the composition"
msgstr ""

#: xfburn/xfburn-data-composition.c:312 xfburn-popup-menus.ui:7
msgid "Create directory"
msgstr ""

#: xfburn/xfburn-data-composition.c:312
msgid "Add a new directory to the composition"
msgstr ""

#: xfburn/xfburn-data-composition.c:317 xfburn/xfburn-audio-composition.c:335
#: xfburn-popup-menus.ui:17 xfburn-popup-menus.ui:25 xfburn-popup-menus.ui:43
msgid "Remove"
msgstr ""

#: xfburn/xfburn-data-composition.c:317 xfburn/xfburn-audio-composition.c:335
msgid "Remove the selected file(s) from the composition"
msgstr ""

#: xfburn/xfburn-data-composition.c:319 xfburn/xfburn-audio-composition.c:337
msgid "Clear"
msgstr ""

#: xfburn/xfburn-data-composition.c:319 xfburn/xfburn-audio-composition.c:337
msgid "Clear the content of the composition"
msgstr ""

#: xfburn/xfburn-data-composition.c:330
msgid "Volume name :"
msgstr ""

#: xfburn/xfburn-data-composition.c:364 xfburn.ui:76
msgid "Contents"
msgstr ""

#: xfburn/xfburn-data-composition.c:379 xfburn/xfburn-directory-browser.c:113
msgid "Size"
msgstr ""

#: xfburn/xfburn-data-composition.c:382
msgid "Local Path"
msgstr ""

#: xfburn/xfburn-data-composition.c:657
msgid "You must give a name to the file."
msgstr ""

#: xfburn/xfburn-data-composition.c:666 xfburn/xfburn-data-composition.c:1124
#: xfburn/xfburn-audio-composition.c:1138
msgid "A file with the same name is already present in the composition."
msgstr ""

#: xfburn/xfburn-data-composition.c:791 xfburn/xfburn-data-composition.c:792
#: xfburn/xfburn-data-composition.c:794
msgid "New directory"
msgstr ""

#. Note to translators: first %s is the date in "i18n" format (year-month-day), %d is a running number of compositions
#: xfburn/xfburn-data-composition.c:967
#, c-format
msgid "Data %s~%d"
msgstr ""

#: xfburn/xfburn-data-composition.c:1203
#, c-format
msgid ""
"%s cannot be added to the composition, because it exceeds the maximum "
"allowed file size for iso9660."
msgstr ""

#: xfburn/xfburn-data-composition.c:1210
#, c-format
msgid ""
"%s is larger than what iso9660 level 2 allows. This can be a problem for old "
"systems or software."
msgstr ""

#: xfburn/xfburn-data-composition.c:1294 xfburn/xfburn-audio-composition.c:1310
msgid "Adding home directory"
msgstr ""

#: xfburn/xfburn-data-composition.c:1295 xfburn/xfburn-audio-composition.c:1311
msgid ""
"You are about to add your home directory to the composition. This is likely "
"to take a very long time, and also to be too big to fit on one disc.\n"
"\n"
"Are you sure you want to proceed?"
msgstr ""

#: xfburn/xfburn-data-composition.c:1463
#, c-format
msgid ""
"A file named \"%s\" already exists in this directory, the file hasn't been "
"added."
msgstr ""

#: xfburn/xfburn-data-composition.c:1865
#, c-format
msgid "%s: null pointer"
msgstr ""

#: xfburn/xfburn-data-composition.c:1867
#, c-format
msgid "%s: out of memory"
msgstr ""

#: xfburn/xfburn-data-composition.c:1869
#, c-format
msgid "%s: node name not unique"
msgstr ""

#: xfburn/xfburn-data-composition.c:1871
#, c-format
msgid "%s: %s (code %X)"
msgstr ""

#. The first string is the renamed name, the second one the original name
#: xfburn/xfburn-data-composition.c:1891
#, c-format
msgid "Duplicate filename '%s' for '%s'"
msgstr ""

#: xfburn/xfburn-data-composition.c:1956
msgid "Error(s) occurred while adding files"
msgstr ""

#: xfburn/xfburn-data-composition.c:1960
msgid "OK"
msgstr ""

#: xfburn/xfburn-device-box.c:142 xfburn/xfburn-device-box.c:143
msgid "Show writers only"
msgstr ""

#: xfburn/xfburn-device-box.c:146
msgid "Show speed selection"
msgstr ""

#: xfburn/xfburn-device-box.c:147
msgid "Show speed selection combo"
msgstr ""

#: xfburn/xfburn-device-box.c:150
msgid "Show mode selection"
msgstr ""

#: xfburn/xfburn-device-box.c:151
msgid "Show mode selection combo"
msgstr ""

#: xfburn/xfburn-device-box.c:154
msgid "Is it a valid combination"
msgstr ""

#: xfburn/xfburn-device-box.c:155
msgid "Is the combination of hardware and disc valid to burn the composition?"
msgstr ""

#: xfburn/xfburn-device-box.c:159
msgid "The blank mode shows different disc status messages than regular mode"
msgstr ""

#: xfburn/xfburn-device-box.c:217
msgid "_Speed:"
msgstr ""

#: xfburn/xfburn-device-box.c:240
msgid "Write _mode:"
msgstr ""

#: xfburn/xfburn-device-box.c:350
msgid "Empty speed list"
msgstr ""

#: xfburn/xfburn-device-box.c:359
msgid ""
"<b>Unable to retrieve the speed list for the drive.</b>\n"
"\n"
"This is a known bug for drives. If you're interested in fixing it, please "
"have a look at the libburn library.\n"
"\n"
"Burning should still work, but if there are problems anyways, please let us "
"know.\n"
"\n"
"<i>Thank you!</i>"
msgstr ""

#: xfburn/xfburn-device-box.c:370
msgid "Continue to _show this notice"
msgstr ""

#: xfburn/xfburn-device-box.c:418
msgid "default"
msgstr ""

#: xfburn/xfburn-device-box.c:449
msgid "Max"
msgstr ""

#: xfburn/xfburn-device-box.c:484
msgid "A full, but erasable disc is in the drive"
msgstr ""

#: xfburn/xfburn-device-box.c:485
msgid ""
"Do you want to blank the disc, so that it can be used for the upcoming burn "
"process?"
msgstr ""

#: xfburn/xfburn-device-box.c:557
msgid "Drive can't burn on the inserted disc"
msgstr ""

#: xfburn/xfburn-device-box.c:566 xfburn/xfburn-device-box.c:612
msgid "Drive is empty"
msgstr ""

#: xfburn/xfburn-device-box.c:569
msgid "Sorry, multisession is not yet supported"
msgstr ""

#: xfburn/xfburn-device-box.c:572
msgid "Inserted disc is full"
msgstr ""

#: xfburn/xfburn-device-box.c:575 xfburn/xfburn-device-box.c:618
msgid "Inserted disc is unsuitable"
msgstr ""

#: xfburn/xfburn-device-box.c:578 xfburn/xfburn-device-box.c:621
msgid "Cannot access drive (it might be in use)"
msgstr ""

#: xfburn/xfburn-device-box.c:583 xfburn/xfburn-device-box.c:624
msgid "Error determining disc"
msgstr ""

#: xfburn/xfburn-device-box.c:604
msgid "Write-once disc, no blanking possible"
msgstr ""

#: xfburn/xfburn-device-box.c:607
msgid "DVD+RW does not need blanking"
msgstr ""

#: xfburn/xfburn-device-box.c:615
msgid "Inserted disc is already blank"
msgstr ""

#: xfburn/xfburn-device-box.c:651
msgid "Automatic"
msgstr ""

#: xfburn/xfburn-device-box.c:658
msgid "With Track Gaps / Track-At-Once (TAO)"
msgstr ""

#: xfburn/xfburn-device-box.c:665
msgid "Multi Session / Session-At-Once (SAO)"
msgstr ""

#: xfburn/xfburn-device-list.c:204 xfburn/xfburn-device-list.c:205
msgid "Number of burners in the system"
msgstr ""

#: xfburn/xfburn-device-list.c:207
msgid "Number of drives in the system"
msgstr ""

#: xfburn/xfburn-device-list.c:208
msgid "Number of drives in the system (readers and writers)"
msgstr ""

#: xfburn/xfburn-device-list.c:210 xfburn/xfburn-device-list.c:211
msgid "List of devices"
msgstr ""

#: xfburn/xfburn-device-list.c:213 xfburn/xfburn-device-list.c:214
msgid "Currently selected device"
msgstr ""

#. globals
#: xfburn/xfburn-directory-browser.c:52
msgid "Folder"
msgstr ""

#: xfburn/xfburn-directory-browser.c:100
msgid "File"
msgstr ""

#: xfburn/xfburn-directory-browser.c:115
msgid "Type"
msgstr ""

#: xfburn/xfburn-fs-browser.c:96 xfburn/xfburn-fs-browser.c:305
msgid "Filesystem"
msgstr ""

#. load the user's home dir
#: xfburn/xfburn-fs-browser.c:284
#, c-format
msgid "%s's home"
msgstr ""

#: xfburn/xfburn-udev-manager.c:495
#, c-format
msgid "Failed to unmount '%s'. Drive cannot be used for burning."
msgstr ""

#. || G_OPTION_FLAG_FILENAME
#: xfburn/xfburn-main.c:74
msgid "Open the burn image dialog, optionally followed by the image filename"
msgstr ""

#: xfburn/xfburn-main.c:76
msgid "Open the blank disc dialog"
msgstr ""

#: xfburn/xfburn-main.c:78
msgid ""
"Start a data composition, optionally followed by files/directories to be "
"added to the composition"
msgstr ""

#: xfburn/xfburn-main.c:80
msgid ""
"Start an audio composition, optionally followed by files/directories to be "
"added to the composition"
msgstr ""

#: xfburn/xfburn-main.c:82
msgid ""
"Select the transcoder, run with --transcoder=list to see the available ones"
msgstr ""

#: xfburn/xfburn-main.c:84
msgid ""
"Start the file browser in the specified directory, or the current directory "
"if none is specified (the default is to start in your home directory)"
msgstr ""

#: xfburn/xfburn-main.c:86
msgid "Display program version and exit"
msgstr ""

#: xfburn/xfburn-main.c:88
msgid ""
"Show main program even when other action is specified on the command line."
msgstr ""

#: xfburn/xfburn-main.c:202 xfburn.desktop.in:5
#: org.xfce.xfburn.appdata.xml.in:8
msgid "Xfburn"
msgstr ""

#: xfburn/xfburn-main.c:211
#, c-format
msgid ""
"%s: %s\n"
"Try %s --help to see a full list of available command line options.\n"
msgstr ""

#: xfburn/xfburn-main.c:220
msgid "Unable to initialize the burning backend."
msgstr ""

#: xfburn/xfburn-main.c:294
msgid "No burners are currently available"
msgstr ""

#: xfburn/xfburn-main.c:296
msgid ""
"Possibly the disc(s) are in use, and cannot get accessed.\n"
"\n"
"If no disc is in the drive, check that you have read and write access to the "
"drive with the current user."
msgstr ""

#: xfburn/xfburn-main.c:329
#, c-format
msgid ""
"Failed to initialize %s transcoder: %s\n"
"\t(falling back to basic implementation)"
msgstr ""

#: xfburn/xfburn-main.c:372
#, c-format
msgid "Image file '%s' does not exist."
msgstr ""

#: xfburn/xfburn-main-window.c:241
msgid "New data composition"
msgstr ""

#: xfburn/xfburn-main-window.c:244
msgid "New audio composition"
msgstr ""

#: xfburn/xfburn-main-window.c:249 xfburn.ui:42
msgid "Blank CD-RW"
msgstr ""

#: xfburn/xfburn-main-window.c:252 xfburn.ui:46
msgid "Burn Image"
msgstr ""

#: xfburn/xfburn-main-window.c:257 xfburn.ui:56
msgid "Refresh"
msgstr ""

#: xfburn/xfburn-main-window.c:257
msgid "Refresh file list"
msgstr ""

#: xfburn/xfburn-main-window.c:511
msgid "Another cd burning GUI"
msgstr ""

#: xfburn/xfburn-notebook-tab.c:81
msgid "Label"
msgstr ""

#: xfburn/xfburn-notebook-tab.c:81
msgid "The text of the label"
msgstr ""

#: xfburn/xfburn-notebook-tab.c:84
msgid "Show close button"
msgstr ""

#: xfburn/xfburn-notebook-tab.c:84
msgid "Determine whether the close button is visible"
msgstr ""

#: xfburn/xfburn-perform-burn.c:103 xfburn/xfburn-perform-burn.c:443
msgid "Formatting..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:269
msgid ""
"Cannot append data to multisession disc in this write mode (use TAO instead)."
msgstr ""

#: xfburn/xfburn-perform-burn.c:273
msgid "Closed disc with data detected, a blank or appendable disc is needed."
msgstr ""

#: xfburn/xfburn-perform-burn.c:275
msgid "No disc detected in drive."
msgstr ""

#: xfburn/xfburn-perform-burn.c:278
msgid "Cannot recognize the state of the drive and disc."
msgstr ""

#: xfburn/xfburn-perform-burn.c:285
msgid "Formatting failed."
msgstr ""

#: xfburn/xfburn-perform-burn.c:303
msgid "There is not enough space available on the inserted disc."
msgstr ""

#: xfburn/xfburn-perform-burn.c:346
#, c-format
msgid "Burning track %2d/%d..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:350 xfburn/xfburn-perform-burn.c:408
msgid "Burning composition..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:377
msgid "standby"
msgstr ""

#: xfburn/xfburn-perform-burn.c:385
msgid "ending"
msgstr ""

#: xfburn/xfburn-perform-burn.c:388
msgid "failing"
msgstr ""

#: xfburn/xfburn-perform-burn.c:391
msgid "unused"
msgstr ""

#: xfburn/xfburn-perform-burn.c:394
msgid "abandoned"
msgstr ""

#: xfburn/xfburn-perform-burn.c:397
msgid "ended"
msgstr ""

#: xfburn/xfburn-perform-burn.c:400
msgid "aborted"
msgstr ""

#: xfburn/xfburn-perform-burn.c:403
msgid "no info"
msgstr ""

#: xfburn/xfburn-perform-burn.c:413
msgid "Writing Lead-In..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:419
msgid "Writing Lead-Out..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:425
msgid "Writing pregap..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:431
msgid "Closing track..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:437
msgid "Closing session..."
msgstr ""

#: xfburn/xfburn-perform-burn.c:456
msgid "see console"
msgstr ""

#: xfburn/xfburn-perform-burn.c:484
msgid "User Aborted"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:117
msgid "Preferences"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:118
msgid "Tune how Xfburn behaves"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:163
#: xfburn/xfburn-preferences-dialog.c:167
msgid "Temporary directory"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:171
msgid "_Clean temporary directory on exit"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:178
msgid "File browser"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:182
msgid "Show _hidden files"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:187
msgid "Show human_readable filesizes"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:197
#: xfburn/xfburn-preferences-dialog.c:208
msgid "General"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:212
#: xfburn/xfburn-preferences-dialog.c:295
msgid "Devices"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:221
msgid "Detected devices"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:243
msgid "Name"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:256
msgid "Revision"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:258
msgid "Node"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:260
msgid "Write CD-R"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:262
msgid "Write CD-RW"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:265
msgid "Write DVD-R"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:268
msgid "Write DVD-RAM"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:271
msgid "Write Blu-ray"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:279
msgid "Sc_an for devices"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:306
msgid "Show warning on _empty speed list"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:315
msgid "FIFO buffer size (in kb)"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:334
msgid "_Close"
msgstr ""

#: xfburn/xfburn-preferences-dialog.c:469
msgid "Changing this setting only takes full effect after a program restart."
msgstr ""

#. label
#: xfburn/xfburn-progress-dialog.c:168 xfburn/xfburn-progress-dialog.c:172
#: xfburn/xfburn-progress-dialog.c:527
msgid "Initializing..."
msgstr ""

#: xfburn/xfburn-progress-dialog.c:187
msgid "Estimated writing speed:"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:190 xfburn/xfburn-progress-dialog.c:206
#: xfburn/xfburn-progress-dialog.c:215 xfburn/xfburn-progress-dialog.c:306
#: xfburn/xfburn-progress-dialog.c:450 xfburn/xfburn-progress-dialog.c:474
#: xfburn/xfburn-progress-dialog.c:498
msgid "unknown"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:201
msgid "FIFO buffer:"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:210
msgid "Device buffer:"
msgstr ""

#. action buttons
#: xfburn/xfburn-progress-dialog.c:220
msgid "_Stop"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:227
msgid "Close"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:348
msgid "Are you sure you want to abort?"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:476
#, c-format
msgid "Min. fill was %2d%%"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:544
msgid "Aborted"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:547
msgid "Formatted."
msgstr ""

#: xfburn/xfburn-progress-dialog.c:556
msgid "Failed"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:559
msgid "Cancelled"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:562
msgid "Completed"
msgstr ""

#: xfburn/xfburn-progress-dialog.c:600
msgid "Aborting..."
msgstr ""

#: xfburn/xfburn-utils.c:132
msgid "Select command"
msgstr ""

#: xfburn/xfburn-utils.c:172
msgid "File(s) to add to composition"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:99
msgid "Welcome to Xfburn!"
msgstr ""

#. buttons
#: xfburn/xfburn-welcome-tab.c:119
msgid "<big>Burn _Image</big>"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:119
msgid "Burn a prepared composition, i.e. an .ISO file"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:124
msgid "<big>New _Data Composition</big>"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:124
msgid "Create a new data disc with the files of your choosing"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:129
msgid "<big>_Blank Disc</big>"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:129
msgid "Prepare the rewriteable disc for a new burn"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:134
msgid "<big>_Audio CD</big>"
msgstr ""

#: xfburn/xfburn-welcome-tab.c:134
msgid "Audio CD playable in regular stereos"
msgstr ""

#. Note to translators: you can probably keep this as gstreamer,
#. * unless you have a good reason to call it by another name that
#. * the user would understand better
#: xfburn/xfburn-audio-composition.c:343 xfburn/xfburn-transcoder-gst.c:581
msgid "gstreamer"
msgstr ""

#: xfburn/xfburn-audio-composition.c:343
msgid "What files can get burned to an audio CD?"
msgstr ""

#: xfburn/xfburn-audio-composition.c:374
msgid "Pos"
msgstr ""

#: xfburn/xfburn-audio-composition.c:376
msgid "Length"
msgstr ""

#: xfburn/xfburn-audio-composition.c:382
msgid "Artist"
msgstr ""

#: xfburn/xfburn-audio-composition.c:395
msgid "Title"
msgstr ""

#: xfburn/xfburn-audio-composition.c:406
msgid "Filename"
msgstr ""

#: xfburn/xfburn-audio-composition.c:557
msgid "Cannot burn audio onto a DVD."
msgstr ""

#: xfburn/xfburn-audio-composition.c:1220
msgid "You can only have a maximum of 99 tracks."
msgstr ""

#: xfburn/xfburn-burn-audio-cd-composition-dialog.c:273
msgid "A problem with the burn backend occurred."
msgstr ""

#: xfburn/xfburn-disc-usage.c:161
msgid "Proceed to Burn"
msgstr ""

#: xfburn/xfburn-disc-usage.c:201
msgid "You are trying to burn more onto the disc than it can hold."
msgstr ""

#. globals
#: xfburn/xfburn-transcoder-basic.c:69 xfburn/xfburn-transcoder-gst.c:145
msgid "An error occurred while setting the burning backend up"
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:129
msgid "basic"
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:135
msgid ""
"The basic transcoder is built in,\n"
"and does not require any library.\n"
"But it can only handle uncompressed\n"
".wav files.\n"
"If you would like to create audio\n"
"compositions from different types of\n"
"audio files, please compile with\n"
"gstreamer support."
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:159
#, c-format
msgid "File %s does not have a .wav extension"
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:164
#, c-format
msgid "File %s does not contain uncompressed PCM wave audio"
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:170
#, c-format
msgid "Could not stat %s: %s"
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:202
#, c-format
msgid "Could not open %s."
msgstr ""

#: xfburn/xfburn-transcoder-basic.c:281
#, c-format
msgid "Could not open %s: %s"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:144
msgid "An error occurred setting gstreamer up for transcoding"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:146
#, c-format
msgid ""
"%s is missing.\n"
"\n"
"You do not have a decoder installed to handle this file.\n"
"Probably you need to look at the gst-plugins-* packages\n"
"for the necessary plugins.\n"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:544
msgid "File content has a decoder but is not audio."
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:587
msgid ""
"The gstreamer transcoder uses the gstreamer\n"
"library for creating audio compositions.\n"
"\n"
"Essentially all audio files should be supported\n"
"given that the correct plugins are installed.\n"
"If an audio file is not recognized, make sure\n"
"that you have the 'good','bad', and 'ugly'\n"
"gstreamer plugin packages installed."
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:627
msgid "No information available on which plugin is required."
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:629
msgid "Required plugins: "
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:664 xfburn/xfburn-transcoder-gst.c:694
#, c-format
msgid "An error occurred while identifying '%s' with gstreamer"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:681
msgid "A plugin"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:816
msgid "Gstreamer did not want to start transcoding (timed out)"
msgstr ""

#: xfburn/xfburn-transcoder-gst.c:851
msgid "Failed to change songs while transcoding"
msgstr ""

#: xfburn/xfburn-transcoder.c:103 xfburn/xfburn-transcoder.c:113
msgid "none"
msgstr ""

#: xfburn/xfburn-transcoder.c:124 xfburn/xfburn-transcoder.c:151
#: xfburn/xfburn-transcoder.c:163
msgid "not implemented"
msgstr ""

#: xfburn/xfburn-device.c:255 xfburn/xfburn-device.c:256
msgid "Display name"
msgstr ""

#: xfburn/xfburn-device.c:258 xfburn/xfburn-device.c:259
msgid "Device address"
msgstr ""

#: xfburn/xfburn-device.c:261
msgid "Device revision"
msgstr ""

#: xfburn/xfburn-device.c:262
msgid "Device Revision"
msgstr ""

#: xfburn/xfburn-device.c:264 xfburn/xfburn-device.c:265
msgid "Burn speeds supported by the device"
msgstr ""

#: xfburn/xfburn-device.c:267 xfburn/xfburn-device.c:268
msgid "Disc status"
msgstr ""

#: xfburn/xfburn-device.c:270 xfburn/xfburn-device.c:271
msgid "Profile no. as reported by libburn"
msgstr ""

#: xfburn/xfburn-device.c:273 xfburn/xfburn-device.c:274
msgid "Profile name as reported by libburn"
msgstr ""

#: xfburn/xfburn-device.c:276 xfburn/xfburn-device.c:277
msgid "Is the disc erasable"
msgstr ""

#: xfburn/xfburn-device.c:279 xfburn/xfburn-device.c:280
msgid "Can burn CDR"
msgstr ""

#: xfburn/xfburn-device.c:282 xfburn/xfburn-device.c:283
msgid "Can burn CDRW"
msgstr ""

#: xfburn/xfburn-device.c:285 xfburn/xfburn-device.c:286
msgid "Can burn DVDR"
msgstr ""

#: xfburn/xfburn-device.c:288 xfburn/xfburn-device.c:289
msgid "Can burn DVDPLUSR"
msgstr ""

#: xfburn/xfburn-device.c:291 xfburn/xfburn-device.c:292
msgid "Can burn DVDRAM"
msgstr ""

#: xfburn/xfburn-device.c:294 xfburn/xfburn-device.c:295
msgid "Can burn Blu-ray"
msgstr ""

#: xfburn/xfburn-device.c:297 xfburn/xfburn-device.c:298
msgid "libburn TAO block types"
msgstr ""

#: xfburn/xfburn-device.c:300 xfburn/xfburn-device.c:301
msgid "libburn SAO block types"
msgstr ""

#: xfburn/xfburn-device.c:303 xfburn/xfburn-device.c:304
msgid "libburn RAW block types"
msgstr ""

#: xfburn/xfburn-device.c:306 xfburn/xfburn-device.c:307
msgid "libburn PACKET block types"
msgstr ""

#: xfburn.ui:5
msgid "_Files"
msgstr ""

#: xfburn.ui:8
msgid "New _data composition"
msgstr ""

#: xfburn.ui:12
msgid "New _audio composition"
msgstr ""

#: xfburn.ui:18
msgid "_Close composition"
msgstr ""

#: xfburn.ui:24
msgid "_Quit"
msgstr ""

#: xfburn.ui:30
msgid "_Edit"
msgstr ""

#: xfburn.ui:33
msgid "Prefere_nce"
msgstr ""

#: xfburn.ui:39
msgid "_Action"
msgstr ""

#: xfburn.ui:53
msgid "_View"
msgstr ""

#: xfburn.ui:62
msgid "Show Toolbars"
msgstr ""

#: xfburn.ui:66
msgid "Show Filebrowser"
msgstr ""

#: xfburn.ui:73
msgid "_Help"
msgstr ""

#: xfburn.ui:81
msgid "About"
msgstr ""

#: xfburn-popup-menus.ui:13
msgid "Rename"
msgstr ""

#: xfburn-popup-menus.ui:33
msgid "Rename Artist"
msgstr ""

#: xfburn-popup-menus.ui:37
msgid "Rename Title"
msgstr ""

#: desktop-integration/thunar-sendto-xfburn.desktop.in:5
msgid "Data Composition"
msgstr ""

#: xfburn.desktop.in:6
msgid "Disk Burning"
msgstr ""

#: xfburn.desktop.in:7
msgid "CD and DVD burning application"
msgstr ""

#: xfburn.desktop.in:21
msgid "Burn Image (xfburn)"
msgstr ""

#: org.xfce.xfburn.appdata.xml.in:9
msgid "A simple CD burning tool"
msgstr ""

#: org.xfce.xfburn.appdata.xml.in:12
msgid ""
"Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can "
"blank CD/DVD(-RW)s, burn and create iso images, audio CDs, as well as burn "
"personal compositions of data to either CD or DVD. It is stable and under "
"ongoing development."
msgstr ""
